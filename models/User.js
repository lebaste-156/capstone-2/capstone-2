const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	firstName: {
		type: String,
		required: [true, 'First Name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	},
	middleInitial: {
		type: String,
		required: [true, 'Middle Initial is Required']
	},
	order: []
})

module.exports = mongoose.model('User', userSchema);