const mongoose = require('mongoose');

let orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: true
	},
	product: {
		type: String,
		required: true
	}

});

module.exports = mongoose.model('Order', orderSchema);