const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
const dotenv = require('dotenv').config();
const userRoutes = require('./routes/users.js');
const productRoutes = require('./routes/products.js');
const orderRoutes = require('./routes/orders.js')




const app = express();
app.use(cors());
app.use(express.urlencoded({extended: true}));
const port = process.env.PORT;
const connection = process.env.MONGO_CONNECTION;

app.use(express.json());
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);




mongoose.connect(connection)
db = mongoose.connection;
db.on('open', () => console.log(`Connected to MongoDB`));


app.get('/', (req, res) => {
	res.send('Hi Sir Marty')
})

app.listen(port, () => {
	console.log(`Server is running on ${port}`)
})