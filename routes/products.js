// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/products.js');
	const route = express.Router();
	const auth = require('../auth')

// Functionalities
	// Create Product [ADMIN]
		route.post('/create', auth.verify, (req, res) => {
			let data = req.body;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.createProduct(data).then(result => res.send(result)) : res.send('Unauthorized User');
			
		});

	// Retrieve All Products [ADMIN]
		route.get('/all', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.getAllProducts().then(result => res.send(result)) : res.send('Unauthorized User');
		});

	// Retrieve All Active Product
		route.get('/active', (req, res) => {
			controller.getAllActive().then(result => res.send(result));
		});

	// Retrieve Single Product
		route.get('/:productId', (req, res) => {
			let productId = req.params.productId;
			controller.getProduct(productId).then(result => res.send(result));
		});

	// Update Product [ADMIN]
		route.put('/update/:productId', auth.verify, (req, res) => {
			let productId = req.params.productId;
			let data = req.body;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.updateProduct(productId, data).then(result => res.send(result)) : res.send('Unauthorized User');
			
		});

	// Archive Product [ADMIN]
		route.put('/archive/:productId', auth.verify, (req, res) => {
			let productId = req.params.productId;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.archiveProduct(productId).then(result => res.send(result)) : res.send('Unauthorized User')	;
		});

	// Unarchive Product [ADMIN]
		route.put('/unarchive/:productId', auth.verify, (req, res) => {
			let productId = req.params.productId;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.unarchiveProduct(productId).then(result => res.send(result)) : res.send('Unauthorized User')	;
		});

	// Delete Product [ADMIN]
		route.delete('/:product', (req, res) => {
			let id = req.params.product;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;

			isAdmin ? controller.deleteProduct(id).then(result => res.send(result)) : res.send('Unauthorized User');
		});

// Expose route
	module.exports = route;