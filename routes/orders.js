const express = require('express');
const controller = require('../controllers/orders.js')
const auth = require('../auth');


const route = express.Router();

// Create Order
	route.post('/create-order', auth.verify, (req, res) => {
		let productInfo = req.body
		let token = req.headers.authorization;
		let userData = auth.decode(token);
		let userId = userData.id;
		let isAdmin = userData.isAdmin;

		!isAdmin ? controller.createOrder(userId, productInfo).then(result => res.send(result)) : res.send('Admin cannot pruchased')
	});

// Retrieve All Orders [ADMIN]
	route.get('/orders', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let userData = auth.decode(token);
		let isAdmin = userData.isAdmin;

		isAdmin ? controller.getAllOrders().then(result => res.send(result)) : res.send('Unauthorized User');
		
	});

// Retrieve Authenticated User's Order
	route.get('/order', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let userData = auth.decode(token);
		let isAdmin = userData.isAdmin;
		controller.authenticatedUsersOrder(userData).then(result => res.send(result))
	})


module.exports = route;