// Dependencies and Modules
const express = require('express');
const controller = require('../controllers/users.js');
const route = express.Router();
const auth = require('../auth')

// Routes
	// Register User
		route.post('/register', (req, res) => {
			let data = req.body;
			controller.createUser(data).then(result => res.send(result));
		});

	// Login User
		route.post('/login', (req, res) => {
			let data = req.body;
			controller.loginUser(data).then(result => res.send(result));
		});

	// Check Email If Already Taken
		route.post('/check-email', (req, res) => {
			let data = req.body;
			controller.checkEmail(data).then(result => res.send(result));
		});

	// Change Password
		route.post('/change-password', (req, res) => {
			let data = req.body;

			controller.changePassword(data).then(result => res.send(result));
		})
		;
	// Retrieve All Users
		route.get('/all', (req, res) => {
			controller.getAllUsers().then(result => res.send(result));
		});

	// Retrieve Single User
		route.get('/user', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let userId = auth.decode(token).id
			controller.getUser(userId).then(result => res.send(result))
		})

	// Set as Admin [ADMIN]
		route.put('/:userId/set-as-admin', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin
			let id = req.params.userId

			isAdmin ? controller.setAdmin(id).then(result => res.send(result)) : res.send('Unauthorized User')
		});

	// Set as Non-Admin [ADMIN]
		route.put('/:userId/set-as-non-admin', auth.verify, (req, res) => {
			let id = req.params.userId;
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin 

			isAdmin ? controller.setNonAdmin(id).then(result => res.send(result)) : res.send('Unauthorized User');
			
		});

	// Delete User
		route.delete('/:userId', (req, res) => {
			let id = req.params.userId;
			controller.deleteUser(id).then(result => res.send(result));
		});

module.exports = route;