const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;

module.exports.createAccessToken = (data) => {
	let userData = {
			id: data._id,
			email: data.email,
			isAdmin: data.isAdmin
	}
	return jwt.sign(userData, secret, {});
};

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		jwt.verify(token, secret, (error, payload) => {
			if (error) {
				return res.send({auth: 'Auth failed'});
			}
			else {
				next();
			}
		})
	} 
	else {
		return res.send({auth: "No token"});
	}
	
};

module.exports.decode = (accessToken) => {
		if (typeof accessToken !== 'undefined') {
			accessToken = accessToken.slice(7, accessToken.length);
			return jwt.verify(accessToken, secret, (err, verified) => {
				if (err) {
					return null
				}
				else {
					return jwt.decode(accessToken, {complete: true}).payload;
				}
			})
		} 
		
		else {
			return null
		}
		return
	}
