// Depedencies and Modules
	const User = require('../models/User.js');
	const bcrypt = require('bcrypt');
	const auth = require('../auth')

// Functionalities
	// Register User
	module.exports.createUser = (data) => {
		let email = data.email;
		let password = data.password;
		let fName = data.firstName;
		let lName = data.lastName;
		let mInitial = data.middleInitial;

		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(password, 10),
			firstName: fName,
			lastName: lName,
			middleInitial: mInitial
		})

		return newUser.save().then((created, error) => {
			if (created) {
				return created
			} 
			else {
				return false
			}
		})
	}


	// Login user
	module.exports.loginUser = (data) => {
		let uEmail = data.email;
		let uPassword = data.password;

		return User.findOne({email: uEmail}).then(result => {
			
			if (result != null) {
				let hashedPass = result.password;
				const isMatched = bcrypt.compareSync(uPassword, hashedPass);

				if (isMatched) {
					return {access: auth.createAccessToken(result)};
				} 
				else {
					return false
				}
			} 

			else {
				return false
			}
		});
	}

	// Change Password
	module.exports.changePassword = (data) => {
		let uEmail = data.email;
		let oldPass = data.password;
		let newPass = data.newPassword

		return User.findOne({email: uEmail}).then((found, not) => {
			if (found) {
				let hashedPass = found.password;
				let isMatched = bcrypt.compareSync(oldPass, hashedPass);
				console.log(isMatched)
				if (isMatched) {
					found.password = bcrypt.hashSync(newPass, 10)
					return found.save().then(result => {
						return true;
					})
				} 
				else {
					return false
				}
			} 

			else {
				return false
			}
		});
	};

	// Check Email if Already Taken
	module.exports.checkEmail = (data) => {
		let email = data.email
		return User.find({email: email}).then((found, not) => {
			if (found.length > 0) {
				return false;
			}
			else {
				return true;
			}
		})
	}

	// Retrieve All Users
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result
		});
	};

	// Retrieve Single User
	module.exports.getUser = (userId) => {
		return User.findById(userId).then(result => {
			return result
		})
	}

	// Set as Admin [ADMIN]
	module.exports.setAdmin = (userId) => {
		let update = {
			isAdmin: true
		}
		
		return User.findByIdAndUpdate(userId, update).then((admin, error) => {
			if (admin) {
				return true;
			} 
			else {
				return false;
			}
		});
	};

	// Set as Non-Admin [ADMIN]
	module.exports.setNonAdmin = (userId) => {
		let update = {
			isAdmin: false
		}

		return User.findByIdAndUpdate(userId, update).then((userUpdated, error) => {
			if (userUpdated) {
				return true;
			} 
			else {
				return false;
			}
		});
	};

	// Delete User

	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((userDeleted, error) => {
			if (userDeleted) {
				return true;
			} 
			else {
				return false;
			}
		});
	};