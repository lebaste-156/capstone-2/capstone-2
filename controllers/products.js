// Dependencies and Modules
const Product = require('../models/Product');

// Functionalities
	// Create Product [ADMIN]
	module.exports.createProduct = (data) => {
		let pName = data.name;
		let pDescription = data.description;
		let pPrice = data.price;

		let newProduct = new Product({
			name: pName,
			description: pDescription,
			price: pPrice
		});

		return newProduct.save().then((productCreated, error) => {
			if (productCreated) {
				return productCreated;
			} 
			else {
				return false;
			}
		});
	};

	// Retrieve All Products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	// Retrieve All Active Products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		});
	};

	// Retrieve Single Product
	module.exports.getProduct = (productId) => {
		return Product.findById(productId).then(result => {
			return result;
		});
	};

	// Update Product [ADMIN]
	module.exports.updateProduct = (productId, update) => {
		let pName = update.name;
		let	pDescription = update.description;
		let pPrice = update.price;

		let updatedProduct = {
			name: pName,
			description: pDescription,
			price: pPrice

		};
		return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdated, error) => {
			if (productUpdated) {
				return productUpdated
			} 
			else {
				return false
			}
		});
	};

	// Archive Product [ADMIN]
	module.exports.archiveProduct = (productId) => {

		let archivedProduct = {
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, archivedProduct).then((productArchived, error) => {
			if (productArchived) {
				return productArchived
			} 
			else {
				return false
			}
		});
	};

	// Unarchive Product [ADMIN]
	module.exports.unarchiveProduct = (productId) => {

		let archivedProduct = {
			isActive: true
		};

		return Product.findByIdAndUpdate(productId, archivedProduct).then((productUnarchived, error) => {
			if (productUnarchived) {
				return productUnarchived
			} 
			else {
				return false
			}
		});
	};

	// Delete Product [ADMIN]
	module.exports.deleteProduct = (productId) => {
		return Product.findByIdAndRemove(productId).then((productDeleted, error) => {
			if (productDeleted) {
				return productDeleted
			} 
			else {
				return false
			}
		});
	};