const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');

// Functionalities
	// Create Order
	module.exports.createOrder = async (userId, productInfo) => {
		let productId = productInfo.productId;
		let quantity = productInfo.quantity;
		let price = []
		await Product.findById(productId).then(result => {
			price.push(result.price)
		})
		let newOrder = new Order({
			totalAmount: price[0]*quantity,
			userId: userId,
			product: productId
		})

		return newOrder.save().then(result => {
			return result;
		})
	};

	// Retrieve All Order [ADMIN]
	module.exports.getAllOrders = () => {
		return Order.find({}).then(result => {
			return result
		});
	};

	// Retrieve Authenticated User's Order
	module.exports.authenticatedUsersOrder = (userData) => {
		let userId = userData.id;
		return Order.find({userId: userId})
	}